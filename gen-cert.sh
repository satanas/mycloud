#!/usr/bin/env bash
set -eu
org=localhost-ca
domain=localhost

#sudo trust anchor --remove ca.crt || true

openssl genpkey -algorithm RSA -out ca.key
openssl req -new -x509 -key ca.key -out ca.crt -subj "/CN=$org/O=$org"

openssl genpkey -algorithm RSA -out "$domain".key
openssl req -new -key "$domain".key -out "$domain".csr -config ssl.conf

openssl x509 -req -in "$domain".csr -days 365 -out "$domain".crt \
    -CA ca.crt -CAkey ca.key -CAcreateserial \
    -extfile 

#    <(cat <<END
#basicConstraints = CA:FALSE
#subjectKeyIdentifier = hash
#authorityKeyIdentifier = keyid,issuer
#subjectAltName = DNS:$domain
#END
#)
#openssl x509 -in "$domain".csr -days 365 -out "$domain".crt \
#    -CA ca.crt -CAkey ca.key -CAcreateserial -config ssl.conf

#sudo trust anchor ca.crt
