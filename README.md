# NextCloud

## Run locally with docker-compose
```
cp .env.example .env
docker-compose up
```

## Prepare external disks

### Partition external disk (NextCloud data)
Both disk primary and replica are configured the same. However, if using a replica,
make sure it has at least the same capacity than the primary disk (or more).

List disk device:
```
fdisk -l
```

Partition disk:
```
sudo parted /dev/sda --script -- mklabel gpt
sudo parted /dev/sda --script -- mkpart primary ext4 1MB 100%
sudo parted /dev/sda --script print
sudo mkfs.ext4 /dev/sda1
```

_Note: The raspberry pi can provide only 1.2A of power, so when using a replica disk we need to
make sure the second disk has its own power supply_.

## Set up SD card (one-time)

### Install image
Use `Raspberry Pi Imager` to install `Raspberry Pi OS Lite (32 bit)`. Once the image is installed, the defautl
user is `pi` and the default password is `raspberry`.

### Install dependencies
```
sudo apt-get update
sudo apt-get full-upgrade
sudo apt-get install hfsprogs hfsutils hfsplus git vim screen
```

### Configure HDMI
To make HDMI work correctly, edit `/boot/config.txt` and uncomment the following attributes:
```
hdmi_safe=1
hdmi_force_hotplug=1
config_hdmi_boost=9
```

### Generate SSH keys
```
ssh-keygen -t rsa
```
Then go to gitlab.com and add this new public key to the profile.

### Set up Raspberry Pi
Open the configuration wizard:
```
sudo raspi-config
```
Then:
1. Change the default password for `pi` user (System Options)
2. Enable SSH server (Interface Options)
3. Update (update raspi-config)
4. Advance Options -> Bootloader Version
5. Reboot

### Install docker
```
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker pi
```

### Install docker-compose
Install dependencies:
```
sudo apt-get install -y libffi-dev libssl-dev python3-dev python3 python3-pip
```

Install rust:
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Install docker-compose:
```
export CRYPTOGRAPHY_DONT_BUILD_RUST=1
pip3 install docker-compose
```

Create symlink:
```
sudo ln -s /home/pi/.local/bin/docker-compose /usr/local/bin/
```

### Clone project from GitLab
```
git clone git@gitlab.com:satanas/mycloud.git
```

### Create .env file with credentials
Create .env file in project folder and put all the credentials

### Add partitions to /etc/fstab
Connect HDDs and find PARTUUID for the new partitions:
```
sudo blkid
```
Then edit /etc/fstab to mount the respective partitions in `/var/nextcloud` and `/var/nextcloud.backup` (see fstab.example)

### Create mount points and adjust permissions
For primary:
```
sudo mkdir -p /var/nextcloud
sudo mount /var/nextcloud
sudo chgrp pi -R /var/nextcloud/
sudo chmod 775 -R /var/nextcloud/
```

For replica:
```
sudo mkdir -p /var/nextcloud.backup
sudo mount /var/nextcloud.backup
sudo chgrp pi -R /var/nextcloud.backup/
sudo chmod 775 -R /var/nextcloud.backup/
```
Note: Mount point permissions are stored in the HDD.

### Enable NextCloud as a system service
Copy the .service file and enable it in systemd:
```
sudo cp nextcloud.service.example /etc/systemd/system/nextcloud.service
sudo systemctl daemon-reload
systemctl enable nextcloud.service
```

## Setup NextCloud

### Create SSL certificate 
Go to `/var/nextcloud/` and execute:
```
openssl req -x509 -out localhost.crt -keyout localhost.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=localhost' -extensions EXT -config <( \
   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
```

### Configure SSL certifica in iOS
Send the certificate via AirDrop to the iOS device, accept it and then install it:

https://apple.stackexchange.com/questions/283348/how-do-i-trust-a-self-signed-certificate-in-ios-10-3

## Manage NextCloud

### Access via SSH
Copy SSH keys from dev environment(Mac) to the Raspberry Pi:
```
ssh-copy-id pi@192.168.1.14
```
Then, access without using password
```
ssh pi@192.168.1.14
```

### Start/stop/status of the service
```
sudo systemctl start nextcloud.service
sudo systemctl stop nextcloud.service
sudo systemctl status nextcloud.service
```

### Restart networking
```
sudo service networking restart
```

### Restart SSH
```
sudo /etc/init.d/ssh restart
```

## Troubleshooting

### Completely uninstall docker
```
sudo apt-get purge -y docker docker-engine docker docker.io docker-ce docker-ce-cli
sudo apt-get autoremove -y --purge docker docker-engine docker docker.io docker-ce 
```

## To-Do
1. Rsync script for replica HDD
2. Test that the data can be used from the replica
3. Log rotation and log clean up
4. Update raspberry pi bootloader

https://www.youtube.com/watch?v=oX-kH2jYvFE
https://pimylifeup.com/raspberry-pi-bootloader/

## References
* https://raamdev.com/2008/mounting-hfs-with-write-access-in-debian/
* https://docs.nextcloud.com/server/13.0.0/admin_manual/configuration_files/big_file_upload_configuration.html
* https://www.php.net/manual/en/install.fpm.configuration.php (bottom of the page)
* https://github.com/imp/dnsmasq/blob/master/dnsmasq.conf.example
* https://www.tutorialspoint.com/unix_commands/dnsmasq.htm
* https://www.digitalocean.com/community/tutorials/how-to-use-rsync-to-sync-local-and-remote-directories
* https://askubuntu.com/questions/935569/how-to-completely-uninstall-docker
* https://devdojo.com/bobbyiliev/how-to-install-docker-and-docker-compose-on-raspberry-pi
* https://www.youtube.com/watch?v=oX-kH2jYvFE
* https://github.com/raspberrypi/rpi-eeprom/blob/master/firmware/release-notes.md
* https://www.linuxcapable.com/how-to-install-rust-on-debian-11/
* http://ifindbug.com/doc/id-44727/name-Solve%20the%20problem%20of%20'error:%20can't%20find%20Rust%20compiler'%20when%20installing%20docker%20compose%20on%20Raspberry%20Pi.html
